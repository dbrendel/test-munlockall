CFLAGS += -Wall -Wextra -Werror -g

SRC = tst-munlockall.c
EXE = $(SRC:.c=)

$(EXE) : $(SRC)

.PHONY : test
test : $(EXE)
	@./$(EXE)

.PHONY : clean
clean:
	$(RM) $(EXE)
