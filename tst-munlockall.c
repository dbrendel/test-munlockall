/* Copyright Dennis Brendel, Red Hat
 * SPDX-License-Identifier: GPL-2.0-only */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/mman.h>

static unsigned long int get_locked_mem (void)
{
    unsigned long int size_locked = 0;

    char *buf = NULL;
    char *start = NULL;
    char *end = NULL;
    FILE *fp = NULL;

    buf = malloc (1024);
    if (buf == NULL)
    {
        perror ("malloc");
        return 0;
    }

    fp = fopen ("/proc/self/status", "r");
    if (fp == NULL)
    {
        perror ("fopen");
        return 0;
    }

    while (fgets (buf, 1024, fp) != NULL)
    {
        start = strstr (buf, "VmLck:");

        if (start == NULL)
        {
            continue;
        }
        else
        {
            while (isdigit (*start) == 0)
            {
                start++;
            }

            end = start;
            while (isdigit (*end) != 0)
            {
                end++;
            }
            *end = '\0';

            size_locked = strtol (start, NULL, 10);
        }
    }

    fclose (fp);

    return size_locked;
}

static int do_test (void)
{
    int ret = 0;
    unsigned long int size_locked = 0;

    size_locked = get_locked_mem ();

    if (size_locked != 0UL)
    {
        ret++;
        fprintf (stderr, "Locked memory after init should be 0 "
                         "but it is %ld\n", size_locked);
    }

    if (mlockall (MCL_CURRENT | MCL_FUTURE) != 0)
    {
        perror ("mlockall");
    }

    size_locked = get_locked_mem ();

    if (size_locked == 0UL)
    {
        ret++;
        fprintf (stderr, "Locked memory after mlockall () should be greater "
                         "than 0, but it is %ld\n", size_locked);
    }

    if (munlockall () != 0)
    {
        perror ("munlockall");
    }

    size_locked = get_locked_mem ();

    if (size_locked != 0UL)
    {
        ret++;
        fprintf (stderr, "Locked memory after munlockall () should be 0 "
                         "but it is %ld\n", size_locked);
    }

    return ret;
}

int main (void)
{
    return do_test ();
}

